package by.itstep.linguaapp.repository;

import by.itstep.linguaapp.entity.UserEntity;
import by.itstep.linguaapp.entity.UserRole;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@SpringBootTest
public class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @Test
    @Transactional
    public void testUpdate() {
        //given
//        UserEntity user = new UserEntity();
//        user.setId(1);
//        user.setName("Bob_updated");
//        user.setEmail("bob@gmail.com");
//        user.setPassword("4444");
//        user.setRole(UserRole.ADMIN);
//        user.setPhone("+375 29 3333333");
//        user.setCountry("RUS");

        //when
//        List<UserEntity> found = userRepository.findAll();
//        System.out.println("FOUND" + found);

//        List<UserEntity> found = userRepository.findAllAdmins();
//        System.out.println("FOUND" + found);

//        List<UserEntity> found = userRepository.findAllByEmailDomain("gmail.com");
//        System.out.println("FOUND" + found);

        userRepository.deleteAllAdmins();
        userRepository.flush();



        //then
    }

}
