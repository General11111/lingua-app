package by.itstep.linguaapp.controller;

import by.itstep.linguaapp.dto.answers.AnswerFullDto;
import by.itstep.linguaapp.dto.answers.AnswerUpdateDto;
import by.itstep.linguaapp.service.AnswerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class AnswerController {

    @Autowired
    private AnswerService answerService;

    @PutMapping("/answers")
    // -> @PreAuthorize("hasRole('ADMIN')")
    private AnswerFullDto update(@Valid @RequestBody AnswerUpdateDto dto) {
        return answerService.update(dto);
    }

    @DeleteMapping("answers/{id}")
    private void delete(@PathVariable Integer id) {
        answerService.delete(id);
    }


}
