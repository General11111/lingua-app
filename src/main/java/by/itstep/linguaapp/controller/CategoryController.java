package by.itstep.linguaapp.controller;

import by.itstep.linguaapp.dto.category.CategoryCreateDto;
import by.itstep.linguaapp.dto.category.CategoryFullDto;
import by.itstep.linguaapp.dto.category.CategoryUpdateDto;
import by.itstep.linguaapp.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @GetMapping("/categories/{id}")
    private CategoryFullDto findById(@PathVariable Integer id) {
        return categoryService.findById(id);
    }

    @GetMapping("/categories")
    private List<CategoryFullDto> findAll() {
        return categoryService.findAll();
    }

    @PostMapping("/categories")
    private CategoryFullDto create(@Valid @RequestBody CategoryCreateDto dto) {
        return categoryService.create(dto);
    }

    @PutMapping("/categories")
    private CategoryFullDto update(@Valid @RequestBody CategoryUpdateDto dto) {
        return categoryService.update(dto);
    }

    @DeleteMapping("/categories/{id}")
    private void delete(@PathVariable Integer id) {
        categoryService.delete(id);
    }

}
