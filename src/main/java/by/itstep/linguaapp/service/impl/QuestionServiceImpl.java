package by.itstep.linguaapp.service.impl;

import by.itstep.linguaapp.dto.answers.AnswerCreateDto;
import by.itstep.linguaapp.dto.question.QuestionCreateDto;
import by.itstep.linguaapp.dto.question.QuestionFullDto;
import by.itstep.linguaapp.dto.question.QuestionShortDto;
import by.itstep.linguaapp.dto.question.QuestionUpdateDto;
import by.itstep.linguaapp.entity.AnswerEntity;
import by.itstep.linguaapp.entity.CategoryEntity;
import by.itstep.linguaapp.entity.QuestionEntity;
import by.itstep.linguaapp.entity.UserEntity;
import by.itstep.linguaapp.exception.AppEntityNotFoundException;
import by.itstep.linguaapp.mapper.QuestionMapper;
import by.itstep.linguaapp.repository.AnswerRepository;
import by.itstep.linguaapp.repository.CategoryRepository;
import by.itstep.linguaapp.repository.QuestionRepository;
import by.itstep.linguaapp.repository.UserRepository;
import by.itstep.linguaapp.security.AuthenticationService;
import by.itstep.linguaapp.service.MailService;
import by.itstep.linguaapp.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.ValidationException;
import java.time.Instant;
import java.util.List;

@Service
public class QuestionServiceImpl implements QuestionService {

    @Autowired
    private QuestionRepository questionRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private AnswerRepository answerRepository;

    @Autowired
    private QuestionMapper questionMapper;

    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private MailService mailService;



    @Override
    @Transactional
    public QuestionFullDto create(QuestionCreateDto dto) {
        throwIfInvalidNumberOfCorrectAnswers(dto);

        QuestionEntity questionToSave = questionMapper.map(dto);

        for (AnswerEntity answer : questionToSave.getAnswers()) {
            answer.setQuestion(questionToSave);
        }

        List<CategoryEntity> categoriesToAdd = categoryRepository.findAllById(dto.getCategoryIds());
        questionToSave.setCategories(categoriesToAdd);

        QuestionEntity savedQuestion = questionRepository.save(questionToSave);


        QuestionFullDto questionDto = questionMapper.map(savedQuestion);
        return questionDto;
    }

    @Override
    @Transactional
    public QuestionFullDto update(QuestionUpdateDto dto) {
        QuestionEntity questionToUpdate = questionRepository.getById(dto.getId());
        if (questionToUpdate == null) {
            throw new AppEntityNotFoundException("QuestionEntity was not found by id: " + dto.getId());
        }
        questionToUpdate.setDescription(dto.getDescription());
        questionToUpdate.setLevel(dto.getLevel());

        QuestionEntity updateQuestion = questionRepository.save(questionToUpdate);
        QuestionFullDto questionDto = questionMapper.map(updateQuestion);
        System.out.println("QuestionServiceImpl -> Question was successfully updated");
        return questionDto;
    }

    @Override
    @Transactional
    public QuestionFullDto findById(Integer id) {
        QuestionEntity foundQuestion = questionRepository.getById(id);
        if (foundQuestion == null) {
            throw new AppEntityNotFoundException("QuestionEntity was not found by id: " + id);
        }
        QuestionFullDto questionDto = questionMapper.map(foundQuestion);
        System.out.println("QuestionServiceImpl -> Question was successfully found");
        return questionDto;
    }

    @Override
    @Transactional
    public List<QuestionShortDto> findAll() {
        List<QuestionEntity> foundEntities = questionRepository.findAll();
        List<QuestionShortDto> foundDtos = questionMapper.map(foundEntities);
        System.out.println("QuestionServiceImpl -> " + foundDtos.size() + " questions were found.");
        return foundDtos;
    }

    @Override
    @Transactional
    public void delete(Integer id) {
        QuestionEntity entityToDelete = questionRepository.getById(id);
        if (entityToDelete == null) {
            throw new AppEntityNotFoundException("QuestionEntity was not found by id: " + id);
        }

        entityToDelete.setDeletedAt(Instant.now());
        for (AnswerEntity answer : entityToDelete.getAnswers()) {
            answer.setDeletedAt(Instant.now());
        }
        questionRepository.save(entityToDelete);

        System.out.println("QuestionServiceImpl -> question was successfully deleted.");

    }

    @Override
    @Transactional
    public boolean checkAnswer(Integer questionId, Integer answerId) {
        AnswerEntity answer = answerRepository.findByIdAndQuestionId(answerId, questionId);
        if (answer == null) {
            throw new AppEntityNotFoundException("Answer was not found by id: " + answerId + " in the question: " + questionId);
        }

        UserEntity user = authenticationService.getAuthenticatedUser();
        user.setLastAnswerDate(Instant.now());
        userRepository.save(user);

        if (answer.getCorrect()) {
            QuestionEntity question = answer.getQuestion();
            question.getUserWhoCompleted().add(user);
            questionRepository.save(question);
            mailService.sendEmail(user.getEmail(), "Good jop! Right answer!");
        }
        return answer.getCorrect();
    }

    @Override
    @Transactional
    public QuestionFullDto getRandomQuestion(Integer categoryId) {
        UserEntity user = authenticationService.getAuthenticatedUser();
        List<QuestionEntity> foundQuestions = questionRepository.findNotCompleted(categoryId, user.getId());

        if (foundQuestions.isEmpty()) {
            throw new AppEntityNotFoundException("Can't find available questions by category id: " + categoryId);
        }
        int randomIndex = (int) (foundQuestions.size() * Math.random());

        QuestionEntity randomQuestion = foundQuestions.get(randomIndex);
        System.out.println("QuestionServiceImpl -> Random question was successfully found");

        return questionMapper.map(randomQuestion);
    }

    private void throwIfInvalidNumberOfCorrectAnswers(QuestionCreateDto dto) {
        int counter = 0;
        for (AnswerCreateDto answer : dto.getAnswers()) {
            if (answer.getCorrect()) {
                counter++;
            }
        }
        if (counter != 1) {
            throw new ValidationException("Question must contains the only one correct answer");
        }
    }
}
