package by.itstep.linguaapp.service.impl;
import by.itstep.linguaapp.repository.UserRepository;
import by.itstep.linguaapp.service.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

@Service
public class MailServiceImpl implements MailService {

    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    private UserRepository userRepository;

    @Override
    public void sendEmail(String email, String message) {
        SimpleMailMessage mail = new SimpleMailMessage();
        mail.setSubject("Email from LinguaApp Team");
        mail.setTo(email);
        mail.setText("Text from Victor: " + message);

        mailSender.send(mail);
    }

    @Override
    @Scheduled(cron = "* * * ? * *")
    public void sendScheduledNotifications() {
        System.out.println("======>START");

        Instant lastAnswerMinDate = Instant.now().minus(3, ChronoUnit.DAYS);
        Instant lastEmailMinDate = Instant.now().minus(3, ChronoUnit.DAYS);

        userRepository.findAll()
                .stream()
                .filter(user -> user.getLastAnswerDate() == null
                        || user.getLastAnswerDate().isBefore(lastAnswerMinDate))
                .filter(user -> user.getLastEmailDate() == null
                        || user.getLastEmailDate().isBefore(lastEmailMinDate))
                .forEach(user -> {
                    // sendEmail(user.getEmail(),"You haven't answered any questions for a long time!");
                    user.setLastEmailDate(Instant.now());
                    userRepository.save(user);
                });
        System.out.println("=======>FINISH");
    }
}

