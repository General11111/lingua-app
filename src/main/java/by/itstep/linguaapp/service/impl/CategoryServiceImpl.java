package by.itstep.linguaapp.service.impl;

import by.itstep.linguaapp.dto.category.CategoryCreateDto;
import by.itstep.linguaapp.dto.category.CategoryFullDto;
import by.itstep.linguaapp.dto.category.CategoryUpdateDto;
import by.itstep.linguaapp.entity.CategoryEntity;
import by.itstep.linguaapp.exception.AppEntityNotFoundException;
import by.itstep.linguaapp.exception.UniqueValuesIsTakenException;
import by.itstep.linguaapp.mapper.CategoryMapper;
import by.itstep.linguaapp.repository.CategoryRepository;
import by.itstep.linguaapp.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private CategoryMapper categoryMapper;

    @Autowired
    private CategoryRepository categoryRepository;

    @Override
    @Transactional
    public CategoryFullDto create(CategoryCreateDto createDto) {
        CategoryEntity entityToSave = categoryMapper.map(createDto);


        List<CategoryEntity> categoryEntities = categoryRepository.findAll();
        for (CategoryEntity categoryEntity : categoryEntities) {
            if (entityToSave.equals(categoryEntity)) {
                throw new UniqueValuesIsTakenException("Category is not taken");
            }
        }


        CategoryEntity savedEntity = categoryRepository.save(entityToSave);
        CategoryFullDto categoryDto = categoryMapper.map(savedEntity);
        System.out.println("CategoryServiceImpl -> Category was successfully created");
        return categoryDto;
    }

    @Override
    @Transactional
    public CategoryFullDto update(CategoryUpdateDto dto) {
        CategoryEntity categoryToUpdate = categoryRepository.getById(dto.getId());
        if (categoryToUpdate == null) {
            throw new AppEntityNotFoundException("CategoryEntity was not found by id: " + dto.getId());
        }
        categoryToUpdate.setName(dto.getName());

        CategoryEntity updatedCategory = categoryRepository.save(categoryToUpdate);
        CategoryFullDto categoryDto = categoryMapper.map(updatedCategory);
        System.out.println("CategoryServiceImpl -> Category was successfully updated");
        return categoryDto;
    }

    @Override
    @Transactional
    public CategoryFullDto findById(int id) {
        CategoryEntity foundCategory = categoryRepository.getById(id);
        if (foundCategory == null) {
            throw new AppEntityNotFoundException("CategoryEntity was not found by id: " + id);
        }
        CategoryFullDto categoryDto = categoryMapper.map(foundCategory);
        System.out.println("CategoryServiceImpl -> Category was successfully found");
        return categoryDto;
    }

    @Override
    @Transactional
    public List<CategoryFullDto> findAll() {
        List<CategoryEntity> foundCategories = categoryRepository.findAll();
        List<CategoryFullDto> dtos = categoryMapper.map(foundCategories);
        System.out.println("CategoryServiceImpl -> " + dtos.size() +  "categories were found");
        return dtos;
    }

    @Override
    @Transactional
    public void delete(int id) {
        CategoryEntity entityToDelete = categoryRepository.getById(id);
        if (entityToDelete == null) {
            throw new AppEntityNotFoundException("CategoryEntity was not found by id: " + id);
        }
        entityToDelete.setDeletedAt(Instant.now());
        categoryRepository.save(entityToDelete);
        System.out.println("CategoryServiceImpl -> Category was successfully deleted");
    }

}

