package by.itstep.linguaapp.exception;

public class UniqueValuesIsTakenException extends RuntimeException{

    public  UniqueValuesIsTakenException(String message) {
        super(message);
    }

}
